package com.siemens;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;


@RestController
@RequestMapping("api")
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class ProductServiceController {
   private static Map<String, String> urlRepo = new HashMap<>();
   
   @Autowired
   private UniqueIDCreator creator;
   
   @Autowired
   private HttpSession session;
   
   static {
	  URL google = new URL();
      google.setId("123");
      google.setName("https://www.google.com/");
      urlRepo.put(google.getId(), google.getName());
      
   }
   @RequestMapping(value = "/urls/{id}", method = RequestMethod.GET)
   public ResponseEntity<Object> getUrl(@PathVariable("id") String id) { 
	   	String jsessionid = session.getId();
	   	System.out.println(jsessionid);
	   	session.setAttribute("xxx", true);
	   	if(session.getAttribute("xxx") == null) {
	   		System.out.println("jssiosn hasnt attr");
	   		session.setAttribute("mauyhasss", true);
	   	}else {
	   		System.out.println("settings attr");
	   		System.out.println("session =" + jsessionid);
	   		Object mauyhas = session.getAttribute("xxx");
	   		System.out.println(mauyhas);
	   	}
	   	
	   
	   if(urlRepo.containsKey(id)) {
		  return new ResponseEntity<>(urlRepo.get(id), HttpStatus.OK);
	  }else {
		  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
   }
   
   @RequestMapping(value = "/create", method = RequestMethod.POST)
   public ResponseEntity<Object> updateProduct(@RequestBody Map<String, Object> payload) { 
	   session.invalidate();
	   String url =String.valueOf( payload.get("value"));
	   if(urlRepo.containsValue(  url  )){
		   return new ResponseEntity<>(HttpStatus.OK);
	   }else {
		   //check auto wire only
		   System.out.println(creator == null);
		   String uid = creator.createID(url);
		   urlRepo.put(uid, url);
		   return new ResponseEntity<>(uid, HttpStatus.OK);
	   }
   }
   @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
   public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody URL url) { 
      if(!urlRepo.containsKey(id))throw new URLNotfoundException();
      urlRepo.remove(id);
      url.setId(id);
      urlRepo.put(id, url.toString());
      return new ResponseEntity<>("Product is updated successfully", HttpStatus.OK);
   }
   
   
}