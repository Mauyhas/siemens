package com.siemens;

public class URL {
	   private String id;
	   private String value;

	   public String getId() {
	      return id;
	   }
	   public void setId(String id) {
	      this.id = id;
	   }
	   public String getName() {
	      return value;
	   }
	   public void setName(String name) {
	      this.value = name;
	   }
	}