package com.siemens;

import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UniqueIDCreator {

	@Autowired
	public UniqueIDCreator() {
	};

	public String createID(String url) {
		return UUID.nameUUIDFromBytes(url.getBytes()).toString();
	}

	@PostConstruct
	private void init(){
		System.out.println("init : UniqueIDCreator");
	}

	public void cleanup() {
		System.out.println("cleanup : UniqueIDCreator");
	}
}
